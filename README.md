# faasg pilot analyses of ATAC H3K4me3 marks
This tiny project is about classifying ATAC and H3K4me3 peak overlaps in liver from Atlantic salmon. The R-script produces "combined_ATAC_H3K4me3_intersect0.1_with_closestgene.txt" that contains CRE element classifications and some info on gene expression across tissues for the closest gene. 

